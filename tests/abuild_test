#!/usr/bin/env atf-sh

. $(atf_get_srcdir)/test_env.sh
init_tests \
	abuild_help \
	abuild_invalid_opt \
	abuild_version \
	abuild_simple_pkg_without_deps \
	abuild_build_fail \
	abuild_invalid_filename \
	abuild_usr_lib64 \
	abuild_dbg_subpackage \
	abuild_SETFATTR_in_dbg \
	abuild_reproducible \
	abuild_checksum_generation \
	abuild_checksum_duplicates \
	abuild_subpkg_dep_leak \
	abuild_py_providers_creation \
	abuild_reject_init_with_improper_shebang \
	abuild_valid_pkgnames \
	abuild_invalid_pkgnames \
	abuild_invalid_subpkgnames \
	abuild_invalid_subpkg_version \
	abuild_multiline_license \
	abuild_git_ceiling

export ABUILD_SHAREDIR=$(atf_get_srcdir)/..
export ABUILD_CONF=/dev/null
export GIT_CONFIG_GLOBAL="$(atf_get_srcdir)/testdata/gitconfig"
export REPODEST="$PWD"/packages

testrepo=$(atf_get_srcdir)/testrepo

# copy keys
init_keys() {
	cp -ra "$(atf_get_srcdir)"/testdata/.abuild "$PWD"
}

abuild_help_body() {
	atf_check -s exit:0 \
		-o match:"usage:" \
		abuild -h
}

abuild_invalid_opt_body() {
	atf_check -s exit:1 \
		-e match:"usage:" \
		abuild -@
}

abuild_version_body() {
	atf_check -s exit:0 \
		-o match:"abuild [0-9]+\.[0-9]+" \
		abuild -V
}

abuild_simple_pkg_without_deps_body() {
	init_keys
	cp -r "$testrepo"/pkg1 .
	cd pkg1
	atf_check -s exit:0 \
		-e not-match:"WARNING" \
		-e not-match:"fatal" \
		abuild
}

abuild_build_fail_body() {
	init_keys
	mkdir buildfail
	cat >buildfail/APKBUILD <<-EOF
		# Maintainer: Joe User <juser@example.com>
		pkgname="buildfail"
		pkgver="1.0"
		pkgrel=0
		pkgdesc="Dummy test package that fails to build"
		url="https://gitlab.alpinelinux.org/alpine/aports"
		arch="noarch"
		license="MIT"
		subpackages="\$pkgname-dev \$pkgname-doc"
		source=""
		prepare() {
			mkdir -p "\$builddir"
		}
		build() {
			false
		}
		package() {
			true
		}
	EOF
	cd buildfail
	atf_check -s exit:1 \
		-e match:"ERROR: buildfail: build failed" \
		abuild
}

abuild_invalid_filename_body() {
	init_keys
	mkdir invalid-filename
	cd invalid-filename
	cat >APKBUILD <<-EOF
		# Maintainer: Joe User <juser@example.com>
		pkgname="invalid-filename"
		pkgver="1.0"
		pkgrel=0
		pkgdesc="Dummy test package that fails to build"
		url="https://gitlab.alpinelinux.org/alpine/aports"
		arch="noarch"
		license="MIT"
		prepare() {
			mkdir -p "\$builddir"
		}
		build() {
			touch $'bad\nfile'
		}
		check() {
			true
		}
		package() {
			mkdir -p "\$pkgdir"
			cp -r * "\$pkgdir"/
		}
	EOF
	atf_check -s exit:1 \
		-e match:"ERROR:.*: Found filenames with newline" \
		abuild
}

abuild_usr_lib64_body() {
	init_keys
	mkdir lib64test
	cd lib64test
	cat >APKBUILD <<-EOF
		# Maintainer: Joe User <juser@example.com>
		pkgname="lib64test"
		pkgver="1.0"
		pkgrel=0
		pkgdesc="Dummy test package"
		url="https://gitlab.alpinelinux.org/alpine/aports"
		arch="noarch"
		license="MIT"
		source=""
		prepare() {
			mkdir -p "\$builddir"
		}
		build() {
			mkdir -p usr/lib64
		}
		check() {
			true
		}

		package() {
			mkdir -p "\$pkgdir"
			cp -r * "\$pkgdir"/
		}
	EOF
	atf_check -s exit:1 \
		-e match:"ERROR:.*: Packages must not put anything under /usr/lib64" \
		abuild

	options=lib64 atf_check -s exit:0 \
		-e match:"Build complete" \
		abuild
}

abuild_dbg_subpackage_body() {
	init_keys
	cp -ra "$testrepo" .
	cd testrepo/dbgpkg
	atf_check -s exit:0 \
		-o match:"hello world" \
		-e match:"Build complete" \
		abuild
	cd ../..

	arch=$(abuild -A)
	tar -zxf "$REPODEST"/testrepo/$arch/dbgpkg-1.0-r0.apk \
		|| atf_fail "failed to extract dbgpkg-1.0-r0.apk"
	if [ -e usr/lib/debug ]; then
		atf_fail "usr/lib/debug should not exist"
	fi
	debuginfo=$(readelf -wk usr/bin/hello | grep '^  Separate debug info file: [^/]*\.debug$')
	debuginfo_file=${debuginfo#"  Separate debug info file: "}
	atf_check -s exit:0 \
		-e match:"nm: usr/bin/hello: no symbols" \
		nm usr/bin/hello

	if ! [ usr/bin/hello -ef usr/bin/hello-hard ]; then
		atf_fail 'hello is not a hardlink of hello-hard'
	fi

	rm -r usr
	tar -xf "$REPODEST"/testrepo/$arch/dbgpkg-dbg-1.0-r0.apk
	if [ -e usr/bin ]; then
		atf_fail "usr/bin should not exist"
	fi

	find usr
	atf_check -s exit:0 \
		-o match:"T main" \
		nm usr/lib/debug/usr/bin/$debuginfo_file

	if [ -e usr/lib/debug/usr/bin/hello-sym.debug ]; then
		atf_fail "usr/lib/debug/usr/bin/hello-sym.debug should not exist"
	fi

	if [ -e usr/lib/debug/usr/bin/hello.debug ] && [ -e usr/lib/debug/usr/bin/hello-hard.debug ]; then
		atf_fail "only one of hello.debug and hello-hard.debug should exist"
	fi
}

abuild_SETFATTR_in_dbg_body() {
	init_keys
	cp -ra "$testrepo"/dbgpkg .
	cd dbgpkg
	SETFATTR=true atf_check -s exit:0 \
		-o match:"hello world" \
		-e match:"Build complete" \
		abuild
}

abuild_reproducible_body() {
	init_keys
	cp -ra "$testrepo" .
	cd testrepo/pkg1

	# set timestamp of APKBUILD to 1 min older than current time
	touch -d @$(( $(date -u +%s) - 60)) APKBUILD

	arch=$(abuild -A)
	pkgs=$(abuild listpkg)

	abuild || atf_fail "first build failed"
	checksums=$(cd "$REPODEST"/testrepo/$arch && md5sum $pkgs)
	echo "$checksums"

	rm -r "$REPODEST"/testrepo
	abuild || atf_fail "rebuild failed"
	checksums2=$(cd "$REPODEST"/testrepo/$arch && md5sum $pkgs)
	echo "$checksums2"

	if [ "$checksums" != "$checksums2" ]; then
		atf_fail "checksums does not match"
	fi
}

abuild_checksum_generation_body() {
	mkdir foo
	cat > foo/APKBUILD <<-EOF
		pkgname="foo"
		pkgver="1.0"
		source="test.txt"
	EOF
	echo "foo" > foo/test.txt
	cd foo
	abuild checksum || atf_fail "checksum generation failed"
	( . ./APKBUILD && echo "$sha512sums" | sed '/^$/d' > sums )
	cat sums
	sha512sum -c sums || atf_fail "checksum mismatch"
}

abuild_checksum_duplicates_body() {
	mkdir -p foo/dir1 foo/dir2
	cat > foo/APKBUILD <<-EOF
		pkgname="foo"
		pkgver="1.0"
		source="dir1/testfile dir2/testfile"
	EOF
	echo "first" > foo/dir1/testfile
	echo "second" > foo/dir2/testfile
	cd foo
	atf_check -s exit:1 \
		-e match:"ERROR:.*duplicate found" \
		abuild checksum
}

abuild_subpkg_dep_leak_body() {
	mkdir -p testrepo/subpkg-dep-leak
	cd testrepo/subpkg-dep-leak
	cat > APKBUILD <<-EOF
		# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
		pkgname="subpkg-dep-leak"
		pkgver=1.0
		pkgrel=0
		pkgdesc="Dummy test package with subpackages and dependencies"
		url="https://gitlab.alpinelinux.org/alpine/aports"
		arch="noarch"
		depends="tar scanelf"
		license="MIT"
		subpackages="\$pkgname-subpkg"
		options="!check"

		package() {
			mkdir -p "\$pkgdir"
		}

		subpkg() {
			depends="sed"
			mkdir -p "\$subpkgdir"
		}
	EOF
	abuild clean unpack prepare build rootpkg

	grep 'depend = tar' pkg/.control.subpkg-dep-leak/.PKGINFO \
		|| atf_fail "subpkg-dep-leak should depend on tar"
	grep 'depend = scanelf' pkg/.control.subpkg-dep-leak/.PKGINFO \
		|| atf_fail "subpkg-dep-leak should depend on scanelf"
	! grep 'depend = sed' pkg/.control.subpkg-dep-leak/.PKGINFO \
		|| atf_fail "subpkg-dep-leak should not depends on sed"

	grep 'depend = sed' pkg/.control.subpkg-dep-leak-subpkg/.PKGINFO \
		|| atf_fail "subpkg-dep-leak-subpkg should depend on sed"

	! grep 'depend = tar' pkg/.control.subpkg-dep-leak-subpkg/.PKGINFO \
		|| atf_fail "subpkg-dep-leak-subpkg should not depend on tar"
}

abuild_py_providers_creation_body() {
	init_keys
	cp -ra "$testrepo" .
	cd testrepo/py3-foo-and-bar
	abuild rootpkg || atf_fail "abuild failed"
	atf_check -s exit:0 \
		-o match:"provides = py3.9:foo=1.0.0-r0" \
		cat pkg/.control.py3-foo-and-bar/.PKGINFO
}

abuild_reject_init_with_improper_shebang_body() {
	mkdir invalid-initd
	cd invalid-initd
	cat >APKBUILD<<-EOF
		# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
		pkgname="invalid-initd"
		pkgver="1.0"
		pkgrel=0
		pkgdesc="Dummy test package"
		url="https://gitlab.alpinelinux.org/alpine/aports"
		arch="noarch"
		license="MIT"
		subpackages=""
		source="test.initd"
		prepare() {
			mkdir -p "\$builddir"
		}
	EOF
	cat >test.initd<<-EOF
	 	#!/bin/sh
		command="test"
	EOF
	abuild checksum

	atf_check -s exit:1 \
		-o match:"OK" \
		-e match:"ERROR.*is not an openrc" \
		abuild unpack
}

abuild_valid_pkgnames_body() {
	cp -ra "$testrepo"/test-pkgname .
	cd test-pkgname
	atf_check -s exit:0 \
		-e match:"Checking sanity" \
		abuild sanitycheck
	TESTNAME=foo atf_check -s exit:0 \
		-e match:"Checking sanity" \
		abuild sanitycheck
	TESTSUBNAME=foo atf_check -s exit:0 \
		-e match:"Checking sanity" \
		abuild sanitycheck
}

abuild_invalid_pkgnames_body() {
	init_keys
	cp -ra "$testrepo"/test-pkgname .
	cd test-pkgname
	TESTNAME="" atf_check -s exit:1 \
		-e match:"Checking sanity" \
		abuild sanitycheck
	TESTNAME="-foo" atf_check -s exit:1 \
		-e match:"Checking sanity" \
		abuild sanitycheck
	TESTNAME="foo with spaces" atf_check -s exit:1 \
		-e match:"Checking sanity" \
		abuild sanitycheck
}

abuild_invalid_subpkgnames_body() {
	init_keys
	cp -ra "$testrepo"/test-pkgname .
	cd test-pkgname
	TESTSUBNAME="" atf_check -s exit:1 \
		-e match:"Checking sanity" \
		abuild sanitycheck
	TESTSUBNAME="-foo" atf_check -s exit:1 \
		-e match:"Checking sanity" \
		abuild sanitycheck
}

abuild_invalid_subpkg_version_body() {
	init_keys
	cp -ra "$testrepo"/test-pkgname .
	cd test-pkgname
	TESTDEPVER="1.0-0" atf_check -s exit:1 \
		-e match:"Invalid version" \
		abuild all
}

abuild_multiline_license_body() {
	cp -ra "$testrepo"/test-licenses .
	cd test-licenses
	abuild rootpkg
	atf_check -o match:'^license = MIT AND GPL-3.0-only OR GPL-3.0-or-later AND BSD-Clause-4$' \
		cat pkg/.control.test-licenses/.PKGINFO
}

abuild_git_ceiling_body() {
	init_keys
	git init
	mkdir git-pkg
	cd git-pkg
	cat >APKBUILD<<-EOF
		# Maintainer: Joe User <juser@example.com>
		pkgname="git-pkg"
		pkgver="1.0"
		pkgrel=0
		pkgdesc="Dummy test package"
		url="https://gitlab.alpinelinux.org/alpine/aports"
		arch="noarch"
		license="MIT"
		prepare() {
			mkdir -p "\$builddir"
		}
		build() {
			git status
		}
		package() {
			mkdir -p "\$pkgdir"
		}
	EOF
	atf_check -s exit:1 \
		-e match:"not a git repository" \
		-e match:"ERROR: git-pkg: build failed" \
		abuild
}
